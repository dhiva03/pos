'use strict';
module.exports = (sequelize, DataTypes) => {
    let Basket = sequelize.define('basket', {
        referenceNumber: {type: DataTypes.BIGINT('255'), allowNull: false},
        customerId: {type: DataTypes.INTEGER, allowNull: false},
        basketSource: {type: DataTypes.INTEGER, allowNull: false},
        lifeCycleStatus: {type: DataTypes.ENUM, allowNull: false, values: ['Open', 'Closed']},
        closedReason: {type: DataTypes.ENUM, allowNull: false, values: ['ConvertedToTran', 'Expired', 'Cancelled']},
        ship: {type: DataTypes.ENUM, allowNull: false, values: ['Yes', 'No']},
        gift: {type: DataTypes.ENUM, allowNull: false, values: ['Yes', 'No']},
        associateId: {type: DataTypes.INTEGER, allowNull: false},
        country: {type: DataTypes.STRING('255'), allowNull: false},
        zipcode: {type: DataTypes.STRING('255'), allowNull: false},
        email: {type: DataTypes.STRING('255'), allowNull: false},
        phoneNumber: {type: DataTypes.STRING('255'), allowNull: false},
        address: {type: DataTypes.STRING('255'), allowNull: false},
    }, {timestamps: true, tableName: 'basket'});
    Basket.associate = (models) => {
        // associations can be defined here
        Basket.hasMany(models.upc_transaction,{foreignKey:'basketId',targetKey: 'id'})
    };
    return Basket;
};