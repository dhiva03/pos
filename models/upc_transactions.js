'use strict';
module.exports = (sequelize, DataTypes) => {
    let upc_transactions = sequelize.define('upc_transaction', {
        upc: {type: DataTypes.BIGINT('255'), allowNull: false},
        basketId: {type: DataTypes.INTEGER, allowNull: false},
        quantity: {type: DataTypes.INTEGER, allowNull: false},
        price: {type: DataTypes.BIGINT('255'), allowNull: false},
    }, {timestamps: true, tableName: 'upc_transaction'});
    upc_transactions.associate = (models) => {
        // associations can be defined here
        upc_transactions.belongsTo(models.basket,{foreignKey:'basketId'})
    };
    return upc_transactions;
};