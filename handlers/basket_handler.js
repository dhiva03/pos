'use strict';
// import the db properties
const db = require('../models/index');
// import the response properties
const response = require('../response');



/* To retrieve the basket information by referenceNumber and also supports Filter
 1)	lifeCycleStatus
 2)	Limit 
 3)	orderBy
 */
module.exports.retrieve_basket = (params) => {
    let result = new Promise((resolve, reject) => {
         if (params.referenceNumber && params.lifeCycleStatus && params.limit && params.orderBy) {
            let orderBy = params.orderBy.split(' ');
            // Filteration Call
            db.basket.findAll({where: {referenceNumber: params.referenceNumber, lifeCycleStatus: params.lifeCycleStatus}, limit: params.limit, order: [[orderBy[0] || 'createdAt', orderBy[1] || 'desc']], include: [{model: db.upc_transaction}]}).then((success) => {
                if (success.length > 0) {
                    resolve(new response.success(200, success));
                } else {
                    reject(new response.error(404, 'referenceNumber not found'));
                }
            }).catch(error => {
                reject(new response.error(500, error));
            });
        } else if (params.referenceNumber) {
            // ReferenceNumber Call
            db.basket.findAll({where: {referenceNumber: params.referenceNumber}, include: [{model: db.upc_transaction}]}).then((success) => {
                if (success.length > 0) {
                    resolve(new response.success(200, success));
                } else {
                    reject(new response.error(404, 'referenceNumber not found'));
                }
            }).catch(error => {
                   reject(new response.error(500, error));
            });
        } else {
            // Bad Request Call
            reject(new response.error(400, 'Bad Request'));
        }
    });
    return result.then((value) => value, (error) => error);
}

/* To update the basket and list of upc_transaction information by basketId */
module.exports.update_basket = (params) => {
        let result = new Promise((resolve, reject) => {
            if (params.basketId && params.upc_transactions.length > 0) {
                   db.sequelize.transaction((t) =>{
                    // chain all your queries here. make sure you return them.
                    return  db.basket.update(params, {where: {id: params.basketId}}, {transaction: t}).then(  (basket) => {
                           return  db.upc_transaction.bulkCreate(params.upc_transactions,{ updateOnDuplicate : ['id','basketId','quantity','price' , 'createdAt','updatedAt'] }, {transaction: t});
                    });
                }).then((result) => {
                    // Transaction has been committed
                    // result is whatever the result of the promise chain returned to the transaction callback
                      resolve(new response.success(200, 'Basket updated successfully'));
                }).catch((err) =>{
                    // Transaction has been rolled back
                    // err is whatever rejected the promise chain returned to the transaction callback
                    reject(new response.error(500, err));
                });
            } else {
                reject(new response.error(400, 'Bad Request'));
            }
        });
        
    return result.then((value) => value, (error) => error);
}


/* To create the basket with list of upc_transaction information */
module.exports.create_basket = (params) => {
    let result = new Promise((resolve, reject) => {
        if (params) {
            // Create Call
            db.basket.create(params, {include: [{model: db.upc_transaction}]}).then((success) => {
                 resolve(new response.success(200, 'Basket created successfully'));
               }).catch((error) => {
                 reject(new response.error(500, error));
            });
        } else {
                reject(new response.error('400', 'Bad Request'));
        }
    });
    return result.then((value) => value, (error) => error);
}

