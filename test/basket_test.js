const assert = require('assert');
const request = require('request');
const expect = require('chai').expect;

describe('Basket Life Cycle Test', function () {
    it('Test the Create Basket API ', async function () {
        const options = {
            uri: 'https://service.us.apiconnect.ibmcloud.com/gws/apigateway/api/db5f2a21643dc2e87a65dce168f2cc99430d14ff11e9dca8f374303950cbab1b/POS/v1.0/basket',
            method: 'POST',
            json: true,
            body: {
                "referenceNumber": 203,
                "customerId": 21,
                "lifeCycleStatus": "Open",
                "closedReason": "",
                "basketSource": 1,
                "ship": "Yes",
                "gift": "Yes",
                "associateId": 21,
                "country": "23",
                "zipcode": "23",
                "email": "karthi@example.com",
                "phoneNumber": "8452635658",
                "address": "madurai",
                "upc_transactions": [{
                    "upc": "243567871459",
                    "quantity": 15,
                    "price": 400
                }, {
                    "upc": "243567871460",
                    "quantity": 16,
                    "price": 240
                }, {
                    "upc": "243567871461",
                    "quantity": 7,
                    "price": 120
                }, {
                    "upc": "243567871462",
                    "quantity": 13,
                    "price": 100
                }, {
                    "upc": "243567871463",
                    "quantity": 13,
                    "price": 60
                }]
            }

        };
        let result = new Promise(resolve => {
            request(options, function (err, httpResponse, body) {
                console.log(JSON.stringify(body));
                resolve(body);
            });
        });
        return await result.then(value => {
            expect(value.statusCode).to.equal(200);
        });
    });
    it('Test the Update Basket API', async function () {
        const options = {
            uri: 'https://service.us.apiconnect.ibmcloud.com/gws/apigateway/api/db5f2a21643dc2e87a65dce168f2cc99430d14ff11e9dca8f374303950cbab1b/POS/v1.0/basket?basketId=1',
            method: 'PATCH',
            json: true,
            body: {
                "basketId": 1,
                "referenceNumber": 203,
                "customerId": 21,
                "lifeCycleStatus": "Open",
                "closedReason": "",
                "basketSource": 1,
                "ship": "Yes",
                "gift": "Yes",
                "associateId": 21,
                "country": "23",
                "zipcode": "23",
                "email": "karthi@example.com",
                "phoneNumber": "8568568547",
                "address": "madurai",
                "upc_transactions": [
                    {
                        "id": 1,
                        "basketId": 1,
                        "upc": "243567871453",
                        "quantity": 15,
                        "price": 4000
                    }, {
                        "id": 1,
                        "basketId": 1,
                        "upc": "243567871454",
                        "quantity": 16,
                        "price": 2400
                    }, {
                        "id": 1,
                        "basketId": 1,
                        "upc": "243567871455",
                        "quantity": 7,
                        "price": 1200
                    }, {
                        "id": 1,
                        "basketId": 1,
                        "upc": "243567871456",
                        "quantity": 13,
                        "price": 1000
                    }, {
                        "id": 1,
                        "basketId": 1,
                        "upc": "243567871457",
                        "quantity": 13,
                        "price": 6000
                    }]
            }
        };
        let result = new Promise(resolve => {
            request(options, function (err, httpResponse, body) {
                console.log(JSON.stringify(body));
                resolve(body);
            });
        });
        return await result.then(value => {
            expect(value.statusCode).to.equal(200);
        });
    });
    it('Test the Retrieve Basket API By Reference Number', async function () {
        const options = {
            uri: 'https://service.us.apiconnect.ibmcloud.com/gws/apigateway/api/db5f2a21643dc2e87a65dce168f2cc99430d14ff11e9dca8f374303950cbab1b/POS/v1.0/basket?referenceNumber=203',
            method: 'GET',
            json: true
        };
        let result = new Promise(resolve => {
            request(options, function (err, httpResponse, body) {
                console.log(JSON.stringify(body));
                resolve(body);
            });
        });
        return await result.then(value => {
            expect(value.statusCode).to.equal(200);
        });
    });
});


