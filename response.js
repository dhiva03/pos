'use strict';
/* Error Response*/
module.exports.error = function (statusCode, errorMessage) {
    this.statusCode = statusCode;
    this.errorMessage = errorMessage;
}
/* Success Response*/
module.exports.success = function (statusCode, body) {
    this.statusCode = statusCode;
    this.body = body;
}
