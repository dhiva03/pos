'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('basket', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            referenceNumber: {
                type: Sequelize.BIGINT('255'),
                allowNull: false

            },
            customerId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            basketSource: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 1
            },
            lifeCycleStatus: {
                type: Sequelize.ENUM,
                allowNull: false,
                values: ['Open', 'Closed']
            }
            ,
            closedReason: {
                type: Sequelize.ENUM,
                allowNull: false,
                values: ['ConvertedToTran', 'Expired', 'Cancelled']
            }
            ,
            ship: {
                type: Sequelize.ENUM,
                allowNull: false,
                values: ['Yes', 'No']
            }
            ,
            gift: {
                type: Sequelize.ENUM,
                allowNull: false,
                values: ['Yes', 'No']
            }
            ,
            associateId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            country: {
                type: Sequelize.STRING('255'),
                allowNull: false
            }
            ,
            zipcode: {
                type: Sequelize.STRING('255'),
                allowNull: false
            }
            ,
            email: {
                type: Sequelize.STRING('255'),
                allowNull: false
            }
            ,
            phoneNumber: {
                type: Sequelize.STRING('255'),
                allowNull: false
            }
            ,
            address: {
                type: Sequelize.STRING('255'),
                allowNull: false
            }
            ,
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
            ,
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('basket');
    }
};